USE bibliotheque;

-- Obtenir le titre et l'année de tous les livres
SELECT titre,annee FROM livres;

-- * -> Selectionner toutes les colonnes d'une table 
SELECT * FROM livres;

-- Quand il y a une ambiguitée sur le nom d'une colonne (le même nom de colonne présent dans plusieurs tables),
-- on ajoute le nom de la table pour lever l'ambiguité => nom_table.nom_colonne
SELECT auteurs.id,auteurs.nom,pays.nom FROM auteurs,pays;

-- On peut mettre dans les colonnes d'un SELECT: une constante ou une colonne qui provient d'un calcul
SELECT titre,'age=',2024-annee FROM livres;

-- DISTINCT -> permet d’éviter les doublons dans les résultats d’un SELECT
SELECT ALL prenom FROM auteurs;	-- 39 lignes
SELECT prenom FROM auteurs;		-- ALL est implicite

SELECT DISTINCT prenom FROM auteurs;		-- 37 lignes

-- On obtient tous les auteurs qui ont un prenom + un nom distinct 
SELECT DISTINCT prenom,nom  FROM auteurs;

-- un alias permet de renommer temporairement une colonne ou une table dans une requête
-- colonne AS alias, AS est optionel => colonne Alias 

-- Alias sur les colonnes
SELECT titre AS titre_livre, 2024-annee AS age FROM livres;
SELECT titre titre_livre, 2024-annee age FROM livres;

-- Alias sur les tables
SELECT a.prenom, a.nom FROM auteurs AS a;
SELECT a.prenom, a.nom FROM auteurs a;

-- Clause WHERE
-- La clause WHERE permet de sélectionner des lignes qui respectent une condition
-- Selection de tous les titres de livre qui sont sortie après 2000
SELECT titre,annee FROM livres WHERE annee > 2000;
-- Selection de tous les titres de livre qui sont sortie en 1964
SELECT titre,annee FROM livres WHERE annee = 1964; 
-- Selection de tous les titres de livre qui ont u age supérieur à 150
SELECT titre, annee FROM livres WHERE 2024-annee >150;
-- Selection de tous les titres de livre qui ne sont sortie en 2005
SELECT titre,annee FROM livres WHERE annee != 2005; 

-- Opérateurs logiques
-- Les opérateurs logiques AND et OR permettent de combiner des conditions

-- condition 1 | condition 2 | AND | OR | XOR
--    V  	   |     V       | V   | V  | F
--    F  	   |     V       | F   | V  | V
--    V  	   |     F       | F   | V  | V
--    F  	   |     F       | F   | F  | F

-- AND
-- avec l'opérateur AND, il faut que toutes les conditions soient vrai pour que la ligne soit sélectionnée
-- Selection des titres, de l'année de sortie du livre qui sont sorties entre 1970 et 1980
SELECT annee, titre FROM livres WHERE annee>1970 AND annee<1980;
-- Selection de tous les titres et l'année de sortie du livre qui sont sortie avant 1960 ET sont des livres policiers
SELECT annee, titre FROM livres WHERE annee <1960 AND genre=1;

-- OR
-- avec l'opérateur OR, il faut qu'une des conditions soient vrai ou les 2 pour que la ligne soit sélectionnée
-- Selection de tous les titres et année de sortie de livre qui sont sortie en 1982, en 1992 ou en 1954
SELECT annee,titre FROM livres WHERE annee=1954 OR annee=1992 OR annee=1982;

-- Selection de tous les titres , année de sortie et genre de livre qui sont sortie après 1980 OU sont des livres policiers ou les 2
SELECT annee, titre,genre FROM livres WHERE annee>1980 OR genre=1;

-- NOT -> inverser la condition
SELECT annee, titre FROM livres WHERE NOT(annee>1900 AND annee<2000);

-- L'opérateur XOR ou exclusif ->  il faut qu'une des conditions soient vrai pour que la ligne soit sélectionnée MAIS PAS les 2
-- Selection de tous les titres , année de sortie et genre de livre qui sont sortie après 1980 OU sont des livres policiers MAIS PAS LES 2
SELECT annee, titre,genre FROM livres WHERE annee>1980 XOR genre=1;

USE world;
-- Afficher toutes les colonnes et toutes les lignes de la table countrylanguage
SELECT * FROM country_language;

-- Afficher le nom des villes et leur population
SELECT name,population FROM city;

-- Afficher la liste des noms des continents sans doublons qui se trouve de la table country
SELECT DISTINCT continent FROM country;

-- Afficher le nom des villes du district de Nagano
SELECT name FROM city WHERE district = 'nagano';

-- Afficher les pays dont la surface est comprise entre 80000 et 100000 km2
SELECT name FROM country WHERE surface_area>80000 AND surface_area<100000;

-- Afficher les noms et la population des villes qui ont un code_country égal à ITA et dont la population est supérieur à 300000 habitants
SELECT name,population  FROM city WHERE country_code = 'ITA' AND population > 300000;

-- Afficher le nom du pays, le continent, espérance de vie et produit national brut (gnp) soit des pays qui ont une espérance de vie supérieur à 80 ans ou les pays européens qui ont un produit national brut supérieur à 1000000 d'euro
SELECT name,continent, life_expectancy,gnp FROM country
WHERE life_expectancy>80 OR (continent='Europe' AND gnp>1000000);

USE bibliotheque;

-- IN
SELECT titre, annee FROM livres WHERE annee IN (1982,1992,1965,1954);
SELECT prenom,nom FROM auteurs WHERE prenom IN ('Pierre','Jack','James');

-- BETWEEN
SELECT titre, annee FROM livres WHERE annee BETWEEN 1980 AND 1990;

SELECT prenom, nom,naissance FROM auteurs WHERE naissance BETWEEN '1930-01-01' AND '1950-01-01';

SELECT prenom, nom FROM auteurs WHERE prenom BETWEEN 'John' AND 'Pierre';

-- LIKE

SELECT titre FROM livres WHERE titre LIKE 'd%';

SELECT titre FROM livres WHERE titre LIKE 'd___%';

SELECT titre FROM livres WHERE titre LIKE 'd___';

SELECT titre FROM livres WHERE titre LIKE '%s';

SELECT titre FROM livres WHERE titre LIKE '% %';



SELECT prenom, nom FROM auteurs WHERE prenom LIKE '%-%'

-- collation -> COLLATE
-- fini par ci -> case insensitive
-- fini par cs -> case sensitive

-- CREATE TABLE articles(
-- 	id INT PRIMARY KEY,
-- 	prix DECIMAL(6,2),
-- 	nom VARChar(40)
-- ) COLLATE=utf8mb4_general_ci;

-- IS NULL  -> pour tester si une valeur est égal à NULL
SELECT prenom, nom FROM auteurs WHERE deces IS NULL;

--  IS NOT NULL -> pour tester si une valeur qui  n'est pas NULL
SELECT prenom, nom FROM auteurs WHERE deces IS NOT NULL;


-- ORDER BY -> trier le résultat
SELECT titre, annee FROM livres ORDER BY annee; -- impliciment ASC -> croissant 
SELECT titre, annee FROM livres ORDER BY titre;

SELECT titre, annee FROM livres ORDER BY annee DESC; -- DESC -> décroissant

SELECT titre, annee FROM livres ORDER BY annee DESC,titre

SELECT titre, annee FROM livres WHERE annee BETWEEN 1960 AND 1980 ORDER BY titre DESC,annee;

-- LIMIT
SELECT annee, titre FROM Livres LIMIT 3;

SELECT annee, titre FROM livres ORDER BY annee LIMIT 5;
SELECT annee, titre FROM livres ORDER BY annee LIMIT 5 OFFSET 3;
SELECT annee, titre FROM livres ORDER BY annee LIMIT 5 OFFSET 3;
SELECT annee, titre FROM livres ORDER BY annee LIMIT 3,5

USE world;
-- Afficher le nom et l'année d'indépendance des pays qui sont devenus indépendant en 1825, 1867, 1963 et  1993
SELECT name, indep_year FROM country WHERE indep_year IN (1825, 1867, 1963, 1993);

-- Afficher le nom, le continent et la population des pays commençant par la lettre c et contenant au moins 6 caractères, classé par ordre décroissant de population
SELECT name, continent, population FROM country WHERE name LIKE 'c_____%' ORDER BY population DESC;

-- Afficher le nom de la 3 ème ville la plus peuplé
SELECT name, population FROM city ORDER BY population DESC LIMIT 1 OFFSET 2;

-- Afficher les pays qui n'ont pas de capital
SELECT name FROM country WHERE capital IS NULL;