USE exemple;

-- Ajouter des données
-- Insérer une ligne en spécifiant toutes les colonnes
INSERT INTO marques VALUES(1,'Marque A','1982-10-23');

-- Insérer une ligne en spécifiant les colonnes souhaitées
INSERT INTO marques(nom_marque,date_creation) 
VALUES('MArque B','1910-01-05');


-- On n'a pas définie nom_marque -> nom_marque ne pas être NULL (colonne NOT NULL)
-- INSERT INTO marques(date_creation) VALUES ('1910-01-05');

-- On n'a pas définie date_creation -> c'est la valeur par défaut NULL qui sera utilisé
INSERT INTO marques(nom_marque) VALUES ('Marque C');

-- Insérer Plusieurs lignes
INSERT INTO marques(nom_marque, date_creation) VALUES 
('Marque D','2000-04-01'),
('Marque E','2010-05-02'),
('Marque F','1970-12-11');

-- Erreur -> La marque avec l'id 100 n'existe pas, la contrainte d'intégritée référentielle (fk_articles_marques)
-- , n'est pas respecté
-- INSERT INTO article (reference, nom_article,prix,poid,emballage,marque) VALUES 
-- (200,'TV 4K',600.0,6000,'CARTON',100);

INSERT INTO article (reference, nom_article,prix,poid,emballage,marque) VALUES 
(200,'TV 4K',600.0,6000,'CARTON',1), 
(201,'TV',300.0,5000,'CARTON',1),
(500,'Souris',30.0,200.0,'PLASTIQUE',5);

-- Supprimer des données => DELETE, TRUNCATE
-- Effacer une ligne dans une table
DELETE FROM marques WHERE id=6;

-- Effacer plusieurs lignes dans une table
DELETE FROM marques WHERE date_creation <'1980-01-01';

-- Les contraintes de clé étrangère
-- DELETE FROM marques WHERE id=1;
-- erreur -> on ne peut pas suprimer la marque avec l'id 1
-- tant qu'il y a des articles qui référence cette marque

-- Supprimer toutes les lignes de la table articles (il n'y a pas de condition)
DELETE FROM article;

-- comme il n'y a plus d'article qui ont la marque 1, on peut supprimer la marque 1 
DELETE FROM marques WHERE id=1;

INSERT INTO fournisseurs(nom) VALUES 
('fournisseur 1'), -- id=1
('fournisseur 2'); -- id=2

-- Supprimer toutes les lignes de la table fournisseurs
DELETE FROM fournisseurs;

-- l'AUTO_INCREMENT n'est pas réinitilisé
INSERT INTO fournisseurs(nom) VALUES 
('fournisseur 1'), -- id=3
('fournisseur 2'); -- id=4

-- TRUNCATE => réinitialise l'auto incrément
SET FOREIGN_KEY_CHECKS = 0;	-- désactiver la vérification des clés étrangères (MYSQL/ MARIADB)
-- Supression de toutes les lignes de la table fournisseurs  => remet la colonne AUTO_INCREMENT à 0
TRUNCATE TABLE fournisseurs;
SET FOREIGN_KEY_CHECKS = 1; -- réactiver la vérification des clés étrangères

-- l'AUTO_INCREMENT est réinitilisé
INSERT INTO fournisseurs(nom) VALUES 
('fournisseur 1'), -- id=1
('fournisseur 2'); -- id=2


INSERT INTO article (reference, nom_article,prix,poid,emballage) VALUES 
(200,'TV 4K',600.0,6000,'CARTON'),
(201,'TV',300.0,5000,'CARTON'),
(500,'Souris',30.0,200.0,'PLASTIQUE'),
(501,'Clavier AZERTY',20.0,400.0,'SANS');

-- Modification des données => UPDATE
-- Modification le nom de l'article et l'emballage qui a pour reference 501
UPDATE article SET nom_article= 'Clavier QWERTY',emballage='CARTON'
WHERE reference =501;

-- Modifier tous les prix inférieur à 20.0 passe à 15.00 euros
UPDATE article SET prix=15.0 WHERE prix<=30;

-- Pas de condition -> tous les prix sont augmentés de 10%
UPDATE article SET prix=prix *1.1;