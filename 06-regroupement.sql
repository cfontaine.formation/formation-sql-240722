USE bibliotheque;

-- Regroupement avec Group By
-- Obtenir le nombre de livre par année
SELECT annee, count(id) FROM livres GROUP BY annee;

-- Obtenir le nombre de livre par genre
SELECT genres.nom,count(livres.id) AS nb_livres FROM livres 
INNER JOIN genres ON genres.id=livres.genre
GROUP BY genre;

-- having -> condition (idem where) après le regroupement, Where se trouve toujours avant le regroupement
SELECT genres.nom,count(livres.id) AS nb_livres FROM livres 
INNER JOIN genres ON genres.id=livres.genre
GROUP BY genre
HAVING nb_livres>5 ORDER BY nb_livres DESC;

-- Obtenir le nombre de livre par auteur
SELECT CONCAT_WS(' ',prenom,nom) AS auteur, count(livres.id) AS nb_livres FROM livres
INNER JOIN livre2auteur ON livres.id=livre2auteur.id_livre 
INNER JOIN auteurs ON livre2auteur.id_auteur = auteurs.id
GROUP BY auteurs.id
ORDER BY nb_livres DESC;

-- Obtenir le nombre de livre par auteur pour les livres policier 
-- pour les auteurs qui ont écrit au moins 2 livres
SELECT CONCAT_WS(' ',prenom,nom) AS auteur, count(livres.id) AS nb_livres FROM livres
INNER JOIN livre2auteur ON livres.id=livre2auteur.id_livre 
INNER JOIN auteurs ON livre2auteur.id_auteur = auteurs.id
WHERE genre =1
GROUP BY auteurs.id
HAVING nb_livres>2;

-- fonction fenetrage
-- Création de la table ventes et ajout de données
USE exemple;

CREATE TABLE ventes(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom_vendeur VARCHAR(30),
	annee INT,
	vente double
);

INSERT INTO ventes(nom_vendeur,annee,vente)
VALUES 
('Paul',2020,10000),
('Patrick',2020,11000),
('Paola',2020,15000),
('Paul',2021,8000),
('Patrick',2021,10000),
('Paola',2021,5000),
('Paul',2022,14000),
('Patrick',2022,5000),
('Paola',2022,16000),
('Paul',2019,12000),
('Patrick',2019,10000),
('Paola',2019,4000),
('Paul',2018,8000),
('Patrick',2018,10000),
('Paola',2018,5000),
('Paul',2017,1000),
('Patrick',2017,5000),
('Paola',2017,6000);

-- Clause over
-- OVER() -> la partition est la table complète
SELECT annee,nom_vendeur ,vente, SUM(vente) OVER() FROM ventes;

-- regroupement sur les années
-- avec un group by
SELECT annee, SUM(vente) FROM ventes GROUP BY annee;

-- avec OVER(PARTITION BY )
-- OVER(PARTITION BY annee) -> les partitions correspondent aux annees
SELECT annee,nom_vendeur ,vente, SUM(vente) OVER(PARTITION BY annee) FROM ventes;

SELECT annee,nom_vendeur ,vente,
SUM(vente) OVER(PARTITION BY annee),
SUM(vente) OVER(),
AVG(vente) OVER (PARTITION BY annee)
FROM ventes;

-- OVER(PARTITION BY nom_vendeur) -> les partitions correspondent à nom_vendeur
SELECT annee, nom_vendeur ,vente ,
sum(vente) OVER(PARTITION BY nom_vendeur) FROM ventes;

SELECT annee, nom_vendeur ,vente ,
sum(vente) OVER(PARTITION BY nom_vendeur ORDER BY vente DESC) FROM ventes;

-- Fonction de fenetrage
-- 1) Fonction d'agrégation: Min,Max,Sum,Avg,Count
SELECT annee,nom_vendeur,vente, 
Sum(vente) OVER(PARTITION BY nom_vendeur) FROM ventes;
-- 2 Fonction de fenetrage de classement, il faut utiliser ORDER BY
-- ROW_NUMBER() -> numérote 1, 2, ... les lignes de la partitions
SELECT annee,nom_vendeur,vente, 
ROW_NUMBER() OVER(PARTITION BY nom_vendeur ORDER BY vente DESC ) FROM ventes;

-- RANK() -> Attribue un rang à chaque ligne de sa partition en fonction de la clause ORDER BY,
--           mais on aura des écarts dans la séquence des valeurs lorsque plusieurs lignes ont le même rang
-- 1 - 2 - 3 - 4 - 4 - 6
SELECT annee,nom_vendeur,vente, 
RANK() OVER(PARTITION BY nom_vendeur ORDER BY vente DESC ) FROM ventes;

-- DENSE_RANK() -> Attribue un rang à chaque ligne de sa partition en fonction de la clause ORDER BY
-- 1 - 2 - 3 - 4 - 4 - 5
SELECT annee,nom_vendeur,vente, 
DENSE_RANK() OVER(PARTITION BY nom_vendeur ORDER BY vente DESC ) FROM ventes;

-- NTILE(n)-> Distribue les lignes de chaque partition de fenêtre dans un nombre spécifié de groupes clas
SELECT annee,nom_vendeur ,vente,
NTILE(3) OVER(PARTITION BY nom_vendeur ORDER BY vente DESC) FROM ventes;

-- PERCENT_RANK() -> Calculer le rang en pourcentage (rank - 1)/(rows - 1)
SELECT annee,nom_vendeur,vente, 
percent_rank() OVER(PARTITION BY nom_vendeur ORDER BY vente DESC ) FROM ventes;

-- CUME_DIST -> Calcule la distribution cumulée d'une valeur dans un ensemble de valeurs
SELECT annee,nom_vendeur,vente, 
CUME_DIST() OVER(PARTITION BY nom_vendeur ORDER BY vente DESC ) FROM ventes;

-- 3) Fonction de fenetrage de valeur 

-- LAG(colonne) -> Renvoie la valeur de la ligne avant la ligne actuelle dans une partition
-- vente	lag(vente)
-- 16000
-- 15000	16000
SELECT annee,nom_vendeur,vente, 
LAG(vente)  OVER(PARTITION BY nom_vendeur ORDER BY vente DESC ) FROM ventes;

SELECT annee,nom_vendeur,vente, 
vente-LAG(vente)  OVER(PARTITION BY nom_vendeur ORDER BY vente DESC ) FROM ventes;

-- LEAD(colonne) -> Renvoie la valeur de la ligne après la ligne actuelle dans une partition
-- vente	lead(vente)
-- 16000	5000
-- 5000		15000
SELECT annee,nom_vendeur,vente, 
LEAD(vente)  OVER(PARTITION BY nom_vendeur ORDER BY vente DESC ) FROM ventes;

-- FIRST_VALUE(colonne) ->Renvoie la valeur de l'expression spécifiée par rapport à la première ligne de la frame
SELECT annee,nom_vendeur,vente, 
first_value(vente)  OVER(PARTITION BY nom_vendeur ) FROM ventes;

-- LAST_VALUE(colonne) -> Renvoie la valeur de l'expression spécifiée par rapport à la dernière ligne de la frame
SELECT annee,nom_vendeur,vente, 
last_value(vente)  OVER(PARTITION BY nom_vendeur  ) FROM ventes;

-- NTH_VALUE(colonne,num_ligne) -> Renvoie la valeur de l'argument de la nème ligne
SELECT annee,nom_vendeur,vente, 
NTH_VALUE(vente,5)  OVER(PARTITION BY nom_vendeur  ) FROM ventes;