-- Commentaire fin de ligne

# Commentaire fin de ligne

/*
Commentaire 
Sur
Plusieurs
lignes
*/

-- Création de la base de données exemple
CREATE DATABASE exemple;

-- Sélectionner la base de données courante
USE exemple;

-- Afficher l'ensemble des bases de données 
SHOW DATABASES;

-- Supprimer un base de donnée
-- DROP DATABASE exemple;

-- Créer une table article
CREATE TABLE articles(
	reference INT,
	description VARCHAR(200),
	prix DECIMAL(6,2)
);

-- Lister les tables de la base de données
SHOW tables;

-- Afficher la description de la table (mysql/mariadb)
DESCRIBE articles;

-- Supprimer la table
DROP TABLE articles;

-- Exercice: créer la table vols
CREATE TABLE vols(
	numero_vol INT,
	heure_depart TIME,
	ville_depart VARCHAR (40),
	heure_arrive TIME,
	ville_arrive VARCHAR (40)
);


-- Modifier une table

-- Renommer une table (mysql/mariadb/IBM DB2)
RENAME TABLE articles TO article;
-- avec Sql Server : EXEC sp_rename 'article','articles';
-- avec PostgreSQL : ALTER TABLE article' RENAME TO articles;
-- avec Oracle: RENAME article TO articles;

-- Ajouter une colonne
ALTER TABLE article ADD poid DOUBLE;
ALTER TABLE article ADD nom VARCHAR(10);

-- Supprimer une colonne
ALTER TABLE article DROP description;

-- Renommer une colonne
ALTER TABLE article CHANGE nom nom_article VARCHAR(10);

-- Modifier le type d'une colonne VARCHAR(10) -> VARCHAR(60) 
ALTER TABLE article MODIFY nom_article VARCHAR(60);

-- Enumération -> ENUM (MYSQL)
ALTER TABLE article ADD emballage ENUM('CARTON','PLASTIQUE','PAPIER','SANS');


-- Interdire les valeurs NULL sur une colonne -> NOT NULL

-- à la création de la table

-- CREATE TABLE article(
-- 	reference INT,
-- 	nom_article VARCHAR(60),
-- 	poid DOUBLE,
-- 	prix DECIMAL(6,2) NOT NULL,
-- 	emballage ENUM('CARTON','PLASTIQUE','PAPIER','SANS')
-- );

-- Ajouter NOT NULL sur une colonne existante
ALTER TABLE article MODIFY prix DECIMAL(6,2) NOT NULL;


-- Valeur par défaut -> DEFAULT

-- à la création de la table

-- CREATE TABLE article(
-- 	reference INT,
-- 	nom_article VARCHAR(60) DEFAULT 'inconue',
-- 	poid DOUBLE,
-- 	prix DECIMAL(6,2) NOT NULL,
-- 	emballage ENUM('CARTON','PLASTIQUE','PAPIER','SANS')
-- );

-- Modifier une colonne pour ajouter une valeur par défaut
ALTER TABLE article MODIFY nom_article VARCHAR(60) DEFAULT 'inconue';


-- Contrainte d'unicité -> UNIQUE

-- à la création de la table
CREATE TABLE stagiaires
(
	id INT,
	prenom VARCHAR(50),
	nom VARCHAR(50),
	email VARCHAR(200) NOT NULL UNIQUE
);

-- Ajouter une contrainte nommé -> un_email_stagiaire
-- Ajouter une contrainte d'unicité
ALTER TABLE stagiaires ADD CONSTRAINT un_email_stagiaire UNIQUE(email);

-- Supprimer une contrainte nommé
ALTER TABLE stagiaires DROP CONSTRAINT un_email_stagiaire;

-- Si la contrainte n'est pas nommé, on utilise le nom de la colonne au lieu du nom de la contraint
ALTER TABLE stagiaires DROP CONSTRAINT email;


-- Clé primaire
-- Une clé primaire est la donnée qui permet d'identifier de manière unique une ligne dans une table

-- à la création de la table
-- CREATE TABLE article(
-- 	reference INT PRIMARY KEY,
-- 	nom_article VARCHAR(60) DEFAULT 'inconue',
-- 	poid DOUBLE,
-- 	prix DECIMAL(6,2) NOT NULL,
-- 	emballage ENUM('CARTON','PLASTIQUE','PAPIER','SANS')
-- );

-- Modifier la table article pour que la colonne reference soit la clé primaire de la table
-- ALTER TABLE article ADD CONSTRAINT pk_article PRIMARY KEY(reference);

-- Pour une clé primaire le nom de la contrainte n'est pas obligatoire, on pourrait l'écrire aussi :
ALTER TABLE article ADD CONSTRAINT PRIMARY KEY(reference);

-- Modifier la table stagiaires pour que la colonne id soit la clé primaire de la table
ALTER TABLE stagiaires ADD CONSTRAINT PRIMARY KEY(id); 

-- On peut utiliser directement PRIMARY KEY pour supprimer la clé primaire
ALTER TABLE article DROP CONSTRAINT PRIMARY KEY;


-- AUTO_INCREMENT -> la valeur de la clé primaire sera incrémentée automatiquement à chaque ajout
-- d’une ligne dans la table, uniquement pour des clés de type entier

-- à la création de la table
-- CREATE TABLE stagiaires
-- (
-- 	id INT PRIMARY KEY AUTO_INCREMENT ,
-- 	prenom VARCHAR(50),
-- 	nom VARCHAR(50),
-- 	email VARCHAR(200) NOT NULL UNIQUE
-- );

-- Modifier la colonne référence pour ajouter AUTO_INCREMENT sur la clé primaire
ALTER TABLE stagiaires MODIFY id INT AUTO_INCREMENT;

ALTER TABLE vols ADD CONSTRAINT PRIMARY KEY(numero_vol);

ALTER TABLE vols MODIFY ville_depart VARCHAR (40) NOT NULL;

ALTER TABLE vols MODIFY ville_arrive VARCHAR (40) NOT NULL;

CREATE TABLE avions(
	numero_avion INT PRIMARY KEY AUTO_INCREMENT ,
	modele VARCHAR(40) NOT NULL,
	capacite SMALLINT NOT NULL 
);

CREATE TABLE pilotes(
	numero_pilote INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(50) NOT NULL,
	nom VARCHAR(50) NOT NULL,
	nombre_heure_vol INT NOT NULL
);

CREATE TABLE marques(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom_marque VARCHAR(60) NOT NULL,
	date_creation DATE
);

-- Relation 1, n -> colonne de jointure

-- entre article et marques 1 - n
-- un article a une marque
-- une marque a plusieurs articles

-- 1-n: On ajoute une colonne qui sera la clé étrangère du coté 1 de la relation
ALTER TABLE article ADD marque INT;

-- Ajouter une contrainte de clé étrangère pour garantir la cohérence des données.
-- clé étrangère -> colonne marque de articles
-- elle fait référence à la clé primaire id de la table marques

ALTER TABLE article 
ADD CONSTRAINT fk_article_marques
FOREIGN KEY (marque)
REFERENCES marques(id);

-- à la création de la table
-- CREATE TABLE article(
-- 	reference INT PRIMARY KEY,
-- 	nom_article VARCHAR(60) DEFAULT 'inconue',
-- 	poid DOUBLE,
-- 	prix DECIMAL(6,2) NOT NULL,
-- 	emballage ENUM('CARTON','PLASTIQUE','PAPIER','SANS'),
-- 	marque INT,
-- 	
-- 	CONSTRAINT fk_article_marques 
-- 	FOREIGN KEY(marque),
-- 	REFERENCES marques(id)
-- );

-- Atelier relation 1-n
-- vols-pilotes
ALTER TABLE vols ADD pilote INT;

ALTER TABLE vols
ADD CONSTRAINT fk_vols_pilotes
FOREIGN KEY(pilote)
REFERENCES pilotes(numero_pilote);

-- vols-avions
ALTER TABLE vols ADD avion INT;

ALTER TABLE vols
ADD CONSTRAINT fk_vols_avions
FOREIGN KEY(avion)
REFERENCES avions(numero_avion);

-- relation n,n ->
CREATE TABLE fournisseurs(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR (50) NOT NULL
);


-- Création de la table de jointure
CREATE TABLE fournisseurs_articles(
	reference_article INT,
	id_fournisseur INT
);

-- Clé étrangères entre article et la table de jointure fournisseurs_articles
ALTER TABLE fournisseurs_articles
ADD CONSTRAINT fk_article_fournisseur
FOREIGN KEY(reference_article)
REFERENCES article(reference);

-- Clé étrangères entre fournisseurs et la table de jointure fournisseurs_articles
ALTER TABLE fournisseurs_articles
ADD CONSTRAINT fk_fournisseurs_article
FOREIGN KEY (id_fournisseur)
REFERENCES fournisseurs(id);

-- Clé primaire composé dans la table de jointure fournisseurs_articles
ALTER TABLE fournisseurs_articles
ADD CONSTRAINT
PRIMARY KEY(reference_article,id_fournisseur);


-- Atelier :pizzeria
CREATE TABLE ingredients(
	numero_ingredient INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(60) NOT NULL 
	);

CREATE TABLE pizzas(
	numero_pizza INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR (50) NOT NULL,
	base ENUM('ROUGE','BLANCHE','ROSE'),
	prix DECIMAL(4,2) DEFAULT 12.00 NOT NULL,
	photo BLOB
);

-- table de jointure
CREATE TABLE pizzas_ingredients(
	numero_pizza INT,
	numero_ingredient INT,
	
	-- clé étrangère entre la table de jointure et ingrédients
	CONSTRAINT fk_pizzas_ingredients
	FOREIGN KEY (numero_pizza)
	REFERENCES pizzas(numero_pizza),
	
	-- clé étrangère entre la table de jointure et ingrédients
	CONSTRAINT fk_ingredients_pizzas
	FOREIGN KEY (numero_ingredient)
	REFERENCES ingredients(numero_ingredient),
	
	-- Création d'une clé primaire composée
	CONSTRAINT
	PRIMARY KEY (numero_pizza,numero_ingredient)
);

-- --------------------------------------------
-- Suite: création des autre tables
CREATE TABLE livreurs(
	numero_livreur INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(50) NOT NULL,
	telephone CHAR(10) NOT NULL
);

CREATE TABLE clients(
	numero_client INT AUTO_INCREMENT PRIMARY KEY ,
	nom VARCHAR(50) NOT NULL,
	adresse CHAR(255) NOT NULL
);

CREATE TABLE commandes(
	numero_commande INT PRIMARY KEY AUTO_INCREMENT,
	heure_commande DATETIME NOT NULL,
	heure_livraison DATETIME NOT NULL,
	livreur INT,
	client INT,
	
	CONSTRAINT fk_commandes_livreurs
	FOREIGN  KEY (livreur)
	REFERENCES livreurs(numero_livreur),
	
	CONSTRAINT fk_commandes_clients
	FOREIGN  KEY (client)
	REFERENCES clients(numero_client)
);

-- Table de jonction entre pizza et commande
CREATE TABLE pizzas_commandes(
	num_pizza INT,
	num_commande INT,
	quantite INT NOT NULL DEFAULT 1,
	
	CONSTRAINT fk_pizzas_commandes
	FOREIGN KEY (num_pizza)
	REFERENCES pizzas(numero_pizza),
	
	CONSTRAINT fk_commandes_pizzas
	FOREIGN KEY (num_commande)
	REFERENCES commandes(numero_commande),
	
	CONSTRAINT PRIMARY KEY (num_pizza,num_commande)
);
