USE exemple;

-- CREATE TEMPORARY TABLE -> table temporaire
-- Elle sera supprimée dés que l'on quitte la session
CREATE TEMPORARY TABLE utilisateurs
(
	id int PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(100) NOT NULL,
	prenom VARCHAR(100) NOT NULL,
	email VARCHAR(150) NOT NULL
);

-- la table est visible avec SHOW TABLES, mais pas avec Dbeaver
SHOW tables;

INSERT INTO utilisateurs(nom,prenom,email) VALUES
('profit','jim','jprofit@dawan.com');

-- Affiche tous les utilisateurs
SELECT * FROM utilisateurs;

-- On se deconnecte du SGBD -> La table n'existe plus
SELECT * FROM utilisateurs;

-- table CTE -> table à usage unique
USE bibliotheque;

-- La table CTE doit être utilisée immédiatement après sa déclaration, sinon cela provoque une erreur
WITH auteur_vivant_cte AS(
	SELECT id,prenom, nom, naissance FROM auteurs
	WHERE deces IS NULL
)	
-- SELECT * FROM livres; -- --> erreur
SELECT prenom, nom, naissance FROM auteur_vivant_cte WHERE naissance >'1950-01-01';

-- On peut déclarer plusieurs table CTE à la suite avant de les utiliser
WITH auteur_vivant_cte AS(
	SELECT auteurs.id,Concat(prenom,' ', auteurs.nom) AS auteur, naissance,
	pays.nom AS nationalite
	FROM auteurs
	INNER JOIN pays ON auteurs.nation=pays.id
	WHERE deces IS NULL
),
auteur_vivant_usa AS( 
	SELECT id,auteur, naissance FROM auteur_vivant_cte
	WHERE nationalite='états-unis'
	)
SELECT auteur, naissance FROM auteur_vivant_usa
WHERE naissance >'1948-01-01';	

-- Création d'une table à partir d'un SELECT
CREATE TABLE livre_policier
SELECT livres.id,titre,annee FROM livres
INNER JOIN genres ON genres.id = livres.genre
WHERE genres.nom='policier';

SHOW tables;

SELECT * FROM livre_policier;

DROP TABLE livre_policier;

-- Création d'une table temporaire à partir d'un SELECT
CREATE TEMPORARY TABLE auteur_france
SELECT auteurs.id,CONCAT_WS(' ',auteurs.prenom,auteurs.nom) AS auteur,
if(deces IS NULL,
round(datediff(current_date() , naissance)/365,0), 
round(datediff(deces  , naissance)/365,0))
AS age ,
deces IS NULL AS est_vivant
FROM auteurs
INNER JOIN pays ON pays.id=auteurs.nation
WHERE pays.nom='france';

SELECT * FROM auteur_france WHERE est_vivant = 0 AND age<60;

-- Exercice table CTE 
USE world;
-- Tous les pays avec une capitale de plus d'un million d'habitant
WITH city_population_million AS 
( 
	SELECT city.id,city.name,city.population FROM city
	INNER JOIN country ON country.capital=city.ID
	WHERE city.population > 1000000
)
SELECT name,population FROM city_population_million ORDER BY population DESC;


-- Liste des pays ayant une langue parlée par moins de 10% de la population
WITH pays_langue_m_10 AS 
(
	SELECT  DISTINCT country.name
	 FROM countrylanguage
	 INNER JOIN country ON country.code=countrylanguage.CountryCode 
	 WHERE countrylanguage.Percentage  < 10 
)
SELECT name FROM pays_langue_m_10;

/* Créer une table temporaire capitale qui contient;
 - l'id de la ville
 - le nom de la ville
 - la population de la ville
 - le nom du pays
 - le nom du continent
 - la langue officielle la plus parlé dans le pays*/

CREATE TEMPORARY TABLE capitale
SELECT city.id, city.name , city.population, country.name,country.continent,country_language.language FROM city
INNER JOIN country ON country.capital=city.ID 
INNER JOIN country_language  ON country.code =country_language.country_code 
WHERE country_language.percentage=(
	SELECT max(Percentage) FROM countrylanguage c  WHERE CountryCode = country.code
) AND country_language.is_official ='T';

-- utiliser cette table temporaire pour afficher les 10 capitales les + peuplés.
SELECT FROM capitale ORDER BY population DESC LIMIT 10;



