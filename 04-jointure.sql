USE bibliotheque;

-- Jointure interne

-- Relation 1,N
-- On va joindre la table livres et la table genres en utilisant l'égalité entre
-- La clé primaire de genres -> id et la clé étrangère de livre -> genre
SELECT titre, annee,genres.nom AS genre FROM livres
INNER JOIN genres ON livres.genre=genres.id;

SELECT titre, annee,genres.nom AS genre FROM genres
INNER JOIN livres ON livres.genre=genres.id;

-- Jointure entre le table auteurs et la table pays
-- Afficher le prénom et le nom de l'auteur et le nom du pays
SELECT prenom, auteurs.nom, naissance, pays.nom FROM auteurs 
INNER JOIN pays ON auteurs.nation = pays.id;

-- Afficher le titre, l'année de sortie et le genre du livre qui sont sortie entre 1980 et 1990
SELECT titre, annee,genres.nom AS genre FROM livres
INNER JOIN genres ON livres.genre=genres.id WHERE annee BETWEEN 1960 AND 1980 
ORDER BY annee DESC, genres.nom,titre DESC

-- Relation n-n
-- On va joindre : la table livres et la table de jointure livre2auteur
-- On va joindre le resultat de la jointure précédente avec la table auteurs
SELECT titre, prenom, nom FROM livres
INNER JOIN livre2auteur ON livres.id=livre2auteur.id_livre
INNER JOIN auteurs ON livre2auteur.id_auteur=auteurs.id;

SELECT titre,annee, prenom, nom FROM livres
INNER JOIN livre2auteur ON livres.id=livre2auteur.id_livre
INNER JOIN auteurs ON livre2auteur.id_auteur=auteurs.id 
WHERE prenom ='Pierre' AND annee<2000;

-- afficher les 5 auteurs francais ou italien qui ont publié un livre le plus récemment.
SELECT DISTINCT prenom,auteurs.nom  FROM pays 
INNER JOIN auteurs ON pays.id=auteurs.nation
INNER JOIN livre2auteur ON auteurs.id = livre2auteur.id_auteur
INNER JOIN livres ON livre2auteur.id_livre=livres.id
WHERE pays.nom IN('France','Italie') ORDER BY annee DESC LIMIT 5;

USE world;
-- Afficher les nom de pays et le nom de ses villes classer par nom de pays puis par nom de ville en ordre alphabétique
SELECT country.name AS pays,city.name AS villes FROM city
INNER JOIN country ON city.country_code = country.code ORDER BY country.name,city.name; 

-- Afficher: les noms de pays, la langue et le pourcentage classé par pays et par pourcentage décroissant
SELECT name AS pays,LANGUAGE, percentage FROM country
INNER JOIN country_language ON country.code = country_language.country_code 
ORDER BY name, percentage DESC ;

-- Joiture interne avec un SELECT et un WHERE
SELECT titre, nom FROM livres, genres 
WHERE livres.genre=genres.id;

-- Produit cartésien -> CROSS JOIN
-- On obtient toutes les combinaisons possible entre les 2 tables
-- Le nombre d'enregistrements obtenu est égal au nombre de lignes de la première table multiplié par le nombre de lignes de la deuxième table.
USE exemple;

CREATE TABLE plats
(
	id int PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(30)
);

INSERT INTO plats(nom) VALUES
('Ceréale'),
('Pain'),
('Oeuf sur le plat');

CREATE TABLE boissons
(
	id int PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(30)
);

INSERT INTO boissons(nom) VALUES
('thé'),
('café'),
('jus d''orange');

SELECT plats.nom,boissons.nom FROM plats 
CROSS JOIN boissons;

-- Jointure externe LEFT JOIN ou RIGTH JOIN
SELECT genres.nom , titre,annee FROM genres
LEFT JOIN livres ON livres.genre=genres.id;

-- Obtenir tous les genres qui ne sont pas représentés dans la bibliotheque
SELECT genres.nom FROM genres
LEFT JOIN livres ON livres.genre=genres.id WHERE livres.id IS NULL;

-- équivalent avec un right join
SELECT genres.nom ,titre,annee FROM livres 
RIGHT JOIN genres ON livres.genre=genres.id;

SELECT genres.nom ,titre,annee FROM livres 
RIGHT JOIN genres ON livres.genre=genres.id WHERE livres.id IS NULL;

-- Ajouter un livre sans genre (NULL)
INSERT INTO livres(titre,annee) VALUES ('Notre-Dame de Paris',1831);

-- Obtenir les livre qui n'ont pas de genre
SELECT titre, annee FROM livres 
LEFT JOIN genres  ON livres.genre=genres.id WHERE genres.id IS NULL;

-- FULL JOIN
-- SELECT genres.nom, titre FROM livres
-- FULL JOIN genres ON livres.genre=genres.id;

-- FULL JOIN n'est pas encore supporté par MySql/MariaDb
-- Mais on peut le réaliser avec cette requète
SELECT genres.nom , titre,annee FROM genres
LEFT JOIN livres ON livres.genre=genres.id
UNION
SELECT genres.nom ,titre,annee FROM genres 
RIGHT JOIN livres ON livres.genre=genres.id;

-- Obtenir tous les auteurs dont la nationalité n'a pas été renseigné
SELECT prenom,auteurs.nom FROM auteurs 
LEFT JOIN pays ON nation = pays.id WHERE pays.id IS NULL;

-- Obtenir tous les pays qui n'ont pas d'auteur
SELECT pays.nom FROM pays
LEFT JOIN auteurs ON nation = pays.id WHERE auteurs.id IS NULL;

USE world;
-- Afficher le nom des pays sans ville
SELECT country.name FROM country 
LEFT JOIN city ON country.code=city.country_code
WHERE city.id IS NULL;

-- Afficher: les noms de  pays, la  langue et le  pourcentage  classé par pays et par  pourcentage décroissant. mais on ne veut obtenir que les langues officielles
SELECT name AS pays,LANGUAGE, percentage FROM country
INNER JOIN country_language ON country.code = country_language.country_code 
WHERE is_official='T'
ORDER BY name, percentage DESC ;


-- Auto jointure
-- Une table que l'on joindre avec elle même

-- Une auto joiture est souvent utilisé pour représenter
-- une hierarchie en SQL (relation parent->enfant)
USE exemple;
CREATE TABLE salaries(
	id int PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(50) NOT NULL,
	nom VARCHAR(50) NOT NULL,
	manager int ,
	
	CONSTRAINT fk_manager
	FOREIGN KEY (manager)
	REFERENCES salaries(id)
);

INSERT INTO salaries(prenom,nom,manager) VALUES  ('John','Doe',NULL);
INSERT INTO salaries(prenom,nom,manager) VALUES  ('Jane','Doe',1);
INSERT INTO salaries(prenom,nom,manager) VALUES  ('Jo','Dalton',1);
INSERT INTO salaries(prenom,nom,manager) VALUES  ('Alan','Smithee',2);
INSERT INTO salaries(prenom,nom,manager) VALUES  ('Yves','Roulo',2);

SELECT employes.prenom,employes.nom,managers.prenom AS manager_prenom, managers.nom AS manager_nom
FROM salaries AS employes
LEFT JOIN salaries AS managers ON employes.manager=managers.id