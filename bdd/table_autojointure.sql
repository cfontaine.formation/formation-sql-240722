USE exemple;
CREATE TABLE salaries(
	id int PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(50) NOT NULL,
	nom VARCHAR(50) NOT NULL,
	manager int ,
	
	CONSTRAINT fk_manager
	FOREIGN KEY (manager)
	REFERENCES salaries(id)
);

INSERT INTO salaries(prenom,nom,manager) VALUES  ('John','Doe',NULL);
INSERT INTO salaries(prenom,nom,manager) VALUES  ('Jane','Doe',1);
INSERT INTO salaries(prenom,nom,manager) VALUES  ('Jo','Dalton',1);
INSERT INTO salaries(prenom,nom,manager) VALUES  ('Alan','Smithee',2);
INSERT INTO salaries(prenom,nom,manager) VALUES  ('Yves','Roulo',2);