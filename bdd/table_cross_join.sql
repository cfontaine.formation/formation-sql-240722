USE exemple;

CREATE TABLE plats
(
	id int PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(30)
);

INSERT INTO plats(nom) VALUES
('Ceréale'),
('Pain'),
('Oeuf sur le plat');

CREATE TABLE boissons
(
	id int PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(30)
);

INSERT INTO boissons(nom) VALUES
('thé'),
('café'),
('jus d''orange');
