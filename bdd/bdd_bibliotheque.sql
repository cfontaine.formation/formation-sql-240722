-- Base de données: Bibliotheque 
-- (MySQL/MariaDB)
-- Juin 2024
-- -----------------------------

DROP DATABASE IF EXISTS bibliotheque;
CREATE DATABASE IF NOT EXISTS bibliotheque;

USE bibliotheque;

-- Table pays
CREATE TABLE pays (
  id int PRIMARY KEY  AUTO_INCREMENT,
  nom varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO pays (id, nom) VALUES(1, 'États-Unis');
INSERT INTO pays (id, nom) VALUES(2, 'Suède');
INSERT INTO pays (id, nom) VALUES(3, 'Grande-Bretagne');
INSERT INTO pays (id, nom) VALUES(4, 'France');
INSERT INTO pays (id, nom) VALUES(5, 'Espagne');
INSERT INTO pays (id, nom) VALUES(6, 'Italie');
INSERT INTO pays (id, nom) VALUES(7, 'Russie');
INSERT INTO pays (id, nom) VALUES(8, 'Tchécoslovaquie');
INSERT INTO pays (id, nom) VALUES(9, 'Belgique');
INSERT INTO pays (id, nom) VALUES(10, 'Portugal');
INSERT INTO pays (id, nom) VALUES(11, 'Bolivie');
INSERT INTO pays (id, nom) VALUES(12, 'Chine');

-- Table genres
CREATE TABLE genres (
  id int PRIMARY KEY AUTO_INCREMENT,
  nom varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO genres (id, nom) VALUES(1, 'Policier');
INSERT INTO genres (id, nom) VALUES(2, 'Autobiographie');
INSERT INTO genres (id, nom) VALUES(3, 'Horreur');
INSERT INTO genres (id, nom) VALUES(4, 'Fantastique');
INSERT INTO genres (id, nom) VALUES(5, 'Thriller');
INSERT INTO genres (id, nom) VALUES(6, 'Science-fiction');
INSERT INTO genres (id, nom) VALUES(7, 'Drame');
INSERT INTO genres (id, nom) VALUES(8, 'Espionnage');
INSERT INTO genres (id, nom) VALUES(9, 'Historique');
INSERT INTO genres (id, nom) VALUES(10, 'Politique');
INSERT INTO genres (id, nom) VALUES(11, 'Fantasy');
INSERT INTO genres (id, nom) VALUES(12, 'Naturaliste');
INSERT INTO genres (id, nom) VALUES(13, 'Anticipation');
INSERT INTO genres (id, nom) VALUES(14, 'Aventure');
INSERT INTO genres (id, nom) VALUES(15, 'Cyberpunk');
INSERT INTO genres (id, nom) VALUES(16, 'Humour');
INSERT INTO genres (id, nom) VALUES(17, 'Steampunk');
INSERT INTO genres (id, nom) VALUES(18, 'Fable');

-- Table livres
CREATE TABLE livres (
  id int PRIMARY KEY AUTO_INCREMENT,
  titre varchar(240) NOT NULL,
  annee int NOT NULL,
  genre int,
  CONSTRAINT fk_genres
  FOREIGN KEY (genre) REFERENCES genres (id)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO livres (id, titre, annee, genre) VALUES(1, 'Lune sanglante', 1982, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(2, 'Le Dahlia noir', 1987, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(3, 'L.A. Confidential', 1990, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(4, 'American Tabloïd', 1995, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(5, 'American Death Trip', 2001, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(6, 'Underworld USA ', 2009, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(7, 'Ma part d''ombre', 1997, 2);
INSERT INTO livres (id, titre, annee, genre) VALUES(8, 'Carrie', 1974, 3);
INSERT INTO livres (id, titre, annee, genre) VALUES(9, 'Shining,l''enfant lumière	', 1977, 3);
INSERT INTO livres (id, titre, annee, genre) VALUES(10, 'Le Fléau', 1978, 3);
INSERT INTO livres (id, titre, annee, genre) VALUES(11, 'Dead Zone', 1979, 4);
INSERT INTO livres (id, titre, annee, genre) VALUES(12, 'Christine', 1983, 3);
INSERT INTO livres (id, titre, annee, genre) VALUES(13, 'Ça', 1988, 3);
INSERT INTO livres (id, titre, annee, genre) VALUES(14, 'Misery', 1987, 5);
INSERT INTO livres (id, titre, annee, genre) VALUES(15, 'Dune', 1965, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(16, 'Le Messie de Dune', 1969, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(17, 'Les Enfants de Dune', 1978, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(18, 'L''Empereur-Dieu de Dune', 1981, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(19, 'Les Hérétiques de Dune', 1985, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(20, 'La Maison des mères', 1986, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(21, 'Les androïdes rêvent-ils de moutons électriques ?	', 1968, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(22, 'Ubik', 1969, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(23, 'Substance Mort', 1978, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(24, 'Le Maître du Haut Château	', 1962, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(25, 'Les Marteaux de Vulcain', 1975, 4);
INSERT INTO livres (id, titre, annee, genre) VALUES(26, 'Les Hommes qui n''aimaient pas les femmes	', 2005, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(27, 'La Fille qui rêvait d''un bidon d``''essence et d''une allumette', 2005, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(28, 'La Reine dans le palais des courants d''air', 2005, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(29, 'Casino Royale', 1953, 8);
INSERT INTO livres (id, titre, annee, genre) VALUES(30, 'Live and Let Die', 1954, 8);
INSERT INTO livres (id, titre, annee, genre) VALUES(31, 'Moonraker	', 1955, 8);
INSERT INTO livres (id, titre, annee, genre) VALUES(32, 'Diamonds Are Forever', 1956, 8);
INSERT INTO livres (id, titre, annee, genre) VALUES(33, 'From Russia With Love	', 1957, 8);
INSERT INTO livres (id, titre, annee, genre) VALUES(34, 'Dr. No', 1958, 8);
INSERT INTO livres (id, titre, annee, genre) VALUES(35, 'Goldfinger', 1959, 8);
INSERT INTO livres (id, titre, annee, genre) VALUES(36, 'For Your Eyes Only', 1960, 8);
INSERT INTO livres (id, titre, annee, genre) VALUES(37, 'Thunderball', 1961, 8);
INSERT INTO livres (id, titre, annee, genre) VALUES(38, 'The Spy Who Loved Me', 1962, 8);
INSERT INTO livres (id, titre, annee, genre) VALUES(39, 'On Her Majesty''s Secret Service', 1963, 8);
INSERT INTO livres (id, titre, annee, genre) VALUES(40, 'You Only Live Twice', 1964, 8);
INSERT INTO livres (id, titre, annee, genre) VALUES(41, 'The Man With The Golden Gun', 1965, 8);
INSERT INTO livres (id, titre, annee, genre) VALUES(42, 'Octopussy and the Living Daylights', 1966, 8);
INSERT INTO livres (id, titre, annee, genre) VALUES(43, 'Don Quichotte	', 1605, 14);
INSERT INTO livres (id, titre, annee, genre) VALUES(44, 'Le Désert des Tartares', 1940, 7);
INSERT INTO livres (id, titre, annee, genre) VALUES(45, 'Le K', 1966, 4);
INSERT INTO livres (id, titre, annee, genre) VALUES(46, 'Le Nom de la rose', 1980, 7);
INSERT INTO livres (id, titre, annee, genre) VALUES(47, 'Les Cosaques', 1863, 9);
INSERT INTO livres (id, titre, annee, genre) VALUES(48, 'Guerre et Paix', 1865, 9);
INSERT INTO livres (id, titre, annee, genre) VALUES(49, 'Anna Karénine', 1877, 7);
INSERT INTO livres (id, titre, annee, genre) VALUES(50, 'Résurrection', 1899, 10);
INSERT INTO livres (id, titre, annee, genre) VALUES(51, 'Harry Potter à l''école des sorciers', 1997, 11);
INSERT INTO livres (id, titre, annee, genre) VALUES(52, 'Harry Potter et la Chambre des secrets', 1998, 11);
INSERT INTO livres (id, titre, annee, genre) VALUES(53, 'Harry Potter et le Prisonnier d''Azkaban', 1999, 11);
INSERT INTO livres (id, titre, annee, genre) VALUES(54, 'Harry Potter et la Coupe de feu', 1999, 11);
INSERT INTO livres (id, titre, annee, genre) VALUES(55, 'Harry Potter et l''Ordre du phénix', 2003, 11);
INSERT INTO livres (id, titre, annee, genre) VALUES(56, 'Harry Potter et le Prince de sang-mêlé', 2005, 11);
INSERT INTO livres (id, titre, annee, genre) VALUES(57, 'Harry Potter et les Reliques de la Mort', 2007, 11);
INSERT INTO livres (id, titre, annee, genre) VALUES(58, 'Le Fermier Gilles de Ham	', 1949, 11);
INSERT INTO livres (id, titre, annee, genre) VALUES(59, 'Le Hobbit', 1937, 11);
INSERT INTO livres (id, titre, annee, genre) VALUES(60, 'La Communauté de l''Anneau	', 1954, 11);
INSERT INTO livres (id, titre, annee, genre) VALUES(61, 'Les Deux Tours', 1954, 11);
INSERT INTO livres (id, titre, annee, genre) VALUES(62, 'Le Retour du roi', 1955, 11);
INSERT INTO livres (id, titre, annee, genre) VALUES(63, 'La Machine à explorer le temps', 1885, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(64, 'L''Île du docteur Moreau', 1896, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(65, 'L''Homme invisible', 1897, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(66, 'La Guerre des mondes', 1898, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(67, 'La Planète des singes', 1963, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(68, 'L''Appel de la forêt', 1903, 14);
INSERT INTO livres (id, titre, annee, genre) VALUES(69, 'Croc-Blanc', 1923, 14);
INSERT INTO livres (id, titre, annee, genre) VALUES(70, 'Le Bureau des assassinats', 1963, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(71, 'Le Cabaret de la dernière chance', 1913, 2);
INSERT INTO livres (id, titre, annee, genre) VALUES(72, 'L''Appel de Cthulhu', 1928, 4);
INSERT INTO livres (id, titre, annee, genre) VALUES(73, 'L''Affaire Charles Dexter Ward', 1941, 4);
INSERT INTO livres (id, titre, annee, genre) VALUES(74, 'Le Cauchemar d''Innsmouth', 1931, 3);
INSERT INTO livres (id, titre, annee, genre) VALUES(75, 'Celui qui hantait les ténèbres', 1936, 4);
INSERT INTO livres (id, titre, annee, genre) VALUES(76, 'La Peur qui rôde', 1923, 4);
INSERT INTO livres (id, titre, annee, genre) VALUES(77, 'Le Modèle de Pickman', 1926, 4);
INSERT INTO livres (id, titre, annee, genre) VALUES(78, 'La Fortune des Rougon', 1871, 12);
INSERT INTO livres (id, titre, annee, genre) VALUES(79, 'La Curée', 1872, 12);
INSERT INTO livres (id, titre, annee, genre) VALUES(80, 'Le Ventre de Paris', 1873, 12);
INSERT INTO livres (id, titre, annee, genre) VALUES(81, 'La Conquête de Plassans', 1874, 12);
INSERT INTO livres (id, titre, annee, genre) VALUES(82, 'La Faute de l''abbé Mouret', 1875, 12);
INSERT INTO livres (id, titre, annee, genre) VALUES(83, 'Son Excellence Eugène Rougon', 1876, 12);
INSERT INTO livres (id, titre, annee, genre) VALUES(84, 'L''Assommoir', 1878, 12);
INSERT INTO livres (id, titre, annee, genre) VALUES(85, 'Une page d''amour', 1880, 12);
INSERT INTO livres (id, titre, annee, genre) VALUES(86, 'Nana', 1882, 12);
INSERT INTO livres (id, titre, annee, genre) VALUES(87, 'Pot-Bouille', 1883, 12);
INSERT INTO livres (id, titre, annee, genre) VALUES(88, 'Au Bonheur des Dames', 1883, 12);
INSERT INTO livres (id, titre, annee, genre) VALUES(89, 'La Joie de vivre', 1885, 12);
INSERT INTO livres (id, titre, annee, genre) VALUES(90, 'Germinal', 1886, 12);
INSERT INTO livres (id, titre, annee, genre) VALUES(91, 'L''Œuvre', 1887, 12);
INSERT INTO livres (id, titre, annee, genre) VALUES(92, 'La Terre', 1888, 12);
INSERT INTO livres (id, titre, annee, genre) VALUES(93, 'Le Rêve', 1890, 12);
INSERT INTO livres (id, titre, annee, genre) VALUES(94, 'L''Argent	', 1891, 12);
INSERT INTO livres (id, titre, annee, genre) VALUES(95, 'La Débâcle', 1892, 12);
INSERT INTO livres (id, titre, annee, genre) VALUES(96, 'Le Docteur Pascal', 1893, 12);
INSERT INTO livres (id, titre, annee, genre) VALUES(97, 'Neuromancien', 1984, 15);
INSERT INTO livres (id, titre, annee, genre) VALUES(98, 'Comte Zéro', 1986, 15);
INSERT INTO livres (id, titre, annee, genre) VALUES(99, 'Mona Lisa s''éclate', 1988, 15);
INSERT INTO livres (id, titre, annee, genre) VALUES(100, 'L''Atlantide', 1919, 14);
INSERT INTO livres (id, titre, annee, genre) VALUES(101, 'Chroniques martiennes', 1950, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(102, 'Fahrenheit 451', 1955, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(103, 'Le Meilleur des mondes', 1932, 13);
INSERT INTO livres (id, titre, annee, genre) VALUES(104, 'L''Affaire N''Gustro', 1972, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(105, 'Nada', 1972, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(106, 'Morgue pleine', 1972, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(107, 'Que d''os !', 1976, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(108, 'Le Petit Bleu de la côte ouest', 1976, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(109, 'La Position du tireur couché', 1981, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(110, 'Les aventures de Huckleberry Finn', 1884, 14);
INSERT INTO livres (id, titre, annee, genre) VALUES(111, 'Les aventures de Tom Sawyer', 1876, 14);
INSERT INTO livres (id, titre, annee, genre) VALUES(112, 'Le Guide du voyageur galactique', 1982, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(113, 'Le Dernier Restaurant avant la fin du monde', 1982, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(114, 'La Vie, l''Univers et le Reste', 1983, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(115, 'Salut, et encore merci pour le poisson', 1994, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(116, 'Globalement inoffensive', 1994, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(117, 'Le Procès', 1925, 7);
INSERT INTO livres (id, titre, annee, genre) VALUES(118, '1984', 1949, 6);
INSERT INTO livres (id, titre, annee, genre) VALUES(119, 'La Marie du port', 1938, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(120, 'Les Fantômes du chapelier', 1949, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(121, 'L''Aîné des Ferchaux', 1945, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(122, 'La Veuve Couderc', 1942, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(123, 'L''Homme qui regardait passer les trains', 1938, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(124, 'Le Relais d''Alsace', 1931, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(125, 'La Maison du canal', 1933, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(126, 'Le Chien jaune', 1931, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(127, 'Au rendez-vous des Terre-Neuvas', 1931, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(128, 'L''Affaire Saint-Fiacre', 1932, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(129, 'L''Écluse numéro 1', 1933, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(130, 'L''Ombre chinoise', 1931, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(131, 'Un crime en Hollande', 1931, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(132, 'Celle qui n''était plus', 1952, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(133, 'Les Visages de l''ombre', 1953, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(134, 'L''ingénieur aimait trop les chiffres', 1959, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(135, 'D''entre les morts', 1954, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(136, 'Fantômas', 1911, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(137, 'Juve contre Fantômas', 1911, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(138, 'Le Mort qui tue', 1911, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(139, 'L''Assassin de Lady Beltham', 1912, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(140, 'La Cravate de chanvre', 1913, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(141, 'Laissez bronzer les cadavres !', 1971, 1);
INSERT INTO livres (id, titre, annee, genre) VALUES(142, 'Le Samouraï virtuel', 1992, 15);

-- Table auteurs
CREATE TABLE auteurs (
  id int PRIMARY KEY AUTO_INCREMENT,
  prenom varchar(50) NOT NULL,
  nom  varchar(50) NOT NULL,
  naissance date NOT NULL,
  deces date,
  nation int,
  CONSTRAINT fk_nation FOREIGN KEY (nation) REFERENCES pays (id)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(1, 'James', 'Ellroy', '1948-03-04', NULL, 1);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(2, 'Stephen', 'King', '1947-09-21', NULL, 1);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(3, 'Frank', 'Herbert', '1920-10-08', '1986-02-11', 1);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(4, 'Philip K.', 'Dick', '1928-12-16', '1982-03-02', 1);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(5, 'Stieg', 'Larsson', '1954-08-15', '2004-11-09', 2);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(6, 'Ian', 'Fleming', '1908-05-28', '1964-08-12', 3);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(7, 'Pierre Louis', 'Boileau', '1906-04-28', '1989-01-16', 4);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(8, 'Thomas', 'Narcejac', '1908-07-03', '1998-06-07', 4);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(9, 'Miguel', 'de Cervantes', '1547-09-29', '1616-04-23', 5);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(10, 'Dino', 'Buzzati', '1906-10-16', '1972-01-28', 6);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(11, 'Umberto', 'Eco', '1932-01-05', '2016-02-19', 6);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(12, 'Léon', 'Tolstoï', '1828-09-09', '1910-11-20', 7);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(13, 'Pierre', 'Souvestre', '1874-06-01', '1914-02-25', 4);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(14, 'Marcel', 'Allain', '1885-09-15', '1969-08-25', 4);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(15, 'J. K.', 'Rowling', '1965-07-31', NULL, 3);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(16, 'J. R. R.', 'Tolkien', '1892-01-03', '1973-09-02', 3);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(17, 'H. G.', 'Wells', '1866-09-21', '1946-08-13', 3);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(18, 'Pierre', 'Boulle', '1912-02-20', '1994-01-31', 4);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(19, 'Jack', 'London', '1876-01-12', '1916-11-22', 1);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(20, 'H P', 'Lovecraft', '1890-08-20', '1937-03-15', 1);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(21, 'Émile', 'Zola', '1840-04-02', '1902-09-29', 4);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(22, 'William', 'Gibson', '1948-03-17', NULL, 1);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(23, 'Pierre', 'Benoit', '1886-07-16', '1962-03-03', 4);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(24, 'Ray', 'Bradbury', '1920-08-22', '2012-06-05', 1);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(25, 'Aldous', 'Huxley', '1894-07-26', '1963-11-22', 3);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(26, 'Jean-Patrick', 'Manchette', '1942-12-19', '1995-06-03', 4);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(27, 'Jean-Pierre', 'Bastid', '1937-02-04', NULL, 4);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(28, 'Mark', 'Twain', '1835-11-30', '1910-04-21', 1);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(29, 'Douglas', 'Adams', '1952-03-11', '2001-05-11', 3);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(30, 'Franz', 'Kafka	', '1883-07-03', '1924-06-03', 8);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(31, 'George', 'Orwell', '1903-06-25', '1950-01-21', 3);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(32, 'Georges', 'Simenon', '1903-02-13', '1989-09-04', 9);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(33, 'Victor', 'Hugo', '1802-02-26', '1885-05-22', 4);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(34, 'Edgar Allan', 'Poe', '1809-01-19', '1849-10-07', 1);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(35, 'John ', 'Le Carré', '1931-10-19', '2020-12-12', 3);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(36, 'Neal', 'Stephenson', '1959-10-31', NULL, 1);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(37, 'Honoré', 'de Balzac', '1799-05-20', '1850-08-18', NULL);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(38, 'Guy', 'de Maupassant', '1850-08-05', '1893-07-06', NULL);
INSERT INTO auteurs (id, prenom, nom, naissance, deces, nation) VALUES(39, 'Roberto', 'Saviano', '1979-09-22', NULL, NULL);

-- Table livre2auteur
CREATE TABLE livre2auteur (
  id_livre int NOT NULL,
  id_auteur int NOT NULL,
  PRIMARY KEY (id_livre,id_auteur),
  CONSTRAINT fk_auteurs FOREIGN KEY (id_auteur) REFERENCES auteurs (id),
  CONSTRAINT fk_livres FOREIGN KEY (id_livre) REFERENCES livres (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(1, 1);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(2, 1);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(3, 1);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(4, 1);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(5, 1);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(6, 1);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(7, 1);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(8, 2);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(9, 2);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(10, 2);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(11, 2);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(12, 2);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(13, 2);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(14, 2);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(15, 3);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(16, 3);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(17, 3);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(18, 3);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(19, 3);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(20, 3);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(21, 4);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(22, 4);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(23, 4);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(24, 4);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(25, 4);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(26, 5);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(27, 5);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(28, 5);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(29, 6);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(30, 6);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(31, 6);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(32, 6);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(33, 6);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(34, 6);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(35, 6);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(36, 6);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(37, 6);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(38, 6);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(39, 6);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(40, 6);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(41, 6);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(42, 6);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(132, 7);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(133, 7);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(134, 7);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(135, 7);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(132, 8);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(133, 8);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(134, 8);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(135, 8);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(43, 9);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(44, 10);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(45, 10);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(46, 11);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(47, 12);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(48, 12);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(49, 12);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(50, 12);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(136, 13);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(137, 13);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(138, 13);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(139, 13);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(140, 13);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(136, 14);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(137, 14);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(138, 14);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(139, 14);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(140, 14);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(51, 15);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(52, 15);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(53, 15);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(54, 15);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(55, 15);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(56, 15);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(57, 15);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(58, 16);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(59, 16);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(60, 16);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(61, 16);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(62, 16);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(63, 17);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(64, 17);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(65, 17);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(66, 17);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(67, 18);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(68, 19);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(69, 19);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(70, 19);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(71, 19);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(72, 20);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(73, 20);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(74, 20);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(75, 20);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(76, 20);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(77, 20);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(78, 21);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(79, 21);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(80, 21);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(81, 21);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(82, 21);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(83, 21);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(84, 21);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(85, 21);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(86, 21);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(87, 21);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(88, 21);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(89, 21);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(90, 21);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(91, 21);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(92, 21);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(93, 21);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(94, 21);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(95, 21);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(96, 21);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(97, 22);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(98, 22);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(99, 22);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(100, 23);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(101, 24);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(102, 24);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(103, 25);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(104, 26);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(105, 26);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(106, 26);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(107, 26);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(108, 26);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(109, 26);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(141, 26);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(141, 27);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(110, 28);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(111, 28);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(112, 29);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(113, 29);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(114, 29);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(115, 29);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(116, 29);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(117, 30);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(118, 31);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(119, 32);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(120, 32);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(121, 32);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(122, 32);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(123, 32);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(124, 32);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(125, 32);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(126, 32);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(127, 32);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(128, 32);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(129, 32);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(130, 32);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(131, 32);
INSERT INTO livre2auteur (id_livre, id_auteur) VALUES(142, 36);